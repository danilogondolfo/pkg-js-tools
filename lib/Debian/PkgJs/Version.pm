package Debian::PkgJs::Version;

use Exporter 'import';

our $VERSION = '0.15.16';
our @EXPORT = qw($VERSION);

